#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class RemoveParentSpaceUtils:    

    def RemoveParentSpace (self, context):
        print("remove parent space")

        objectParentBoneDictionary = RemoveParentSpaceUtils.FindRelatedObjectsAndBones(self, context)

        bonesToRemoveNDictionary = RemoveParentSpaceUtils.SelectRelatedBones(self, context, objectParentBoneDictionary)

        print("selected pose bones:")
        print(bpy.context.selected_pose_bones)
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        RemoveParentSpaceUtils.RemoveUnwantedBones(self, context, bonesToRemoveNDictionary)

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

    def FindRelatedObjectsAndBones (self, context):
        print("finding related bones")

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #make list out of selection
        selectedBonesPList = bpy.context.selected_pose_bones.copy()
        selectedBonesPList.sort(key = lambda x:len(x.parent_recursive))

        #dictionary containing parent bones list using their object as key
        objectParentBoneDictionary = dict()

        #find parent bones with the suffix ".parent.rig" and add the to objectParentBoneDictionary with their respective object as key
        for boneP in selectedBonesPList:
            print("going through selected bone : " + boneP.name)
            #if boneP contains ".parent.rig"
            if ".parent.rig" in boneP.name:
                #if boneP contains "copy.parent.rig"
                if "copy.parent.rig" in boneP.name:
                    #if boneP's object is already used as key for the objectParentBoneDictionary
                    if boneP.id_data in objectParentBoneDictionary:
                        objectParentBoneDictionary[boneP.id_data.name].append(boneP.name)
                    #if boneP's object is not yet used as key for the objectParentBoneDictionary
                    else:
                        objectParentBoneDictionary[boneP.id_data.name] = [boneP.name]
                    #since boneP contains "copy.parent.rig" it should have a Copy Transform contraint
                    bonePConst = boneP.constraints['Copy Transforms']
                    #add object from boneP's constraint target to objectParentBoneDictionary key
                    if bonePConst.target in objectParentBoneDictionary:
                        objectParentBoneDictionary[bonePConst.target].append(bonePConst.subtarget)
                    else:
                        objectParentBoneDictionary[bonePConst.target] = [bonePConst.subtarget]
                #if boneP does not contain "copy.parent.rig" but still contains "parent.rig"
                else:
                    #add boneP to the objectParentBoneDictionary
                    if boneP.id_data in objectParentBoneDictionary:
                        objectParentBoneDictionary[boneP.id_data.name].append(boneP.name)
                    else:
                        objectParentBoneDictionary[boneP.id_data.name] = [boneP.name]
                    #since boneP is the main parent it should have a ROTF Parent custom property if not it's beceause the main parent affects only bones from the same object
                    if boneP.get('ROTF Parent') != None:
                        #look into custom property for ROTF Parent containing string "objectName1|boneName1?objectName2|boneName2?...
                        objectsBonesList = boneP['ROTF Parent'].split("?")
                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            boneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(boneN)
                            else:
                                objectParentBoneDictionary[objectN] = [boneN]

            #find parent bones through related .child
            if ".child.rig" in boneP.name:
                parentBoneN = boneP.parent.name
                parentBoneP = boneP.parent

                #if boneP's parent contains ".copy.parent.rig"
                if ".copy.parent.rig" in parentBoneN:
                    #find the copy transform constraint of the selected bones containing ".copy.parent.rig" in their name
                    parentBonePConst = boneP.parent.constraints['Copy Transforms']
                    #set aside the main parent's name to which all have been parented to previously
                    mainParentBoneN = parentBonePConst.subtarget

                    #add object from boneP's constraint target to objectParentBoneDictionary key
                    if parentBonePConst.target in objectParentBoneDictionary:
                        objectParentBoneDictionary[parentBonePConst.target.name].append(mainParentBoneN)
                    else:
                        objectParentBoneDictionary[parentBonePConst.target.name] = [mainParentBoneN]

                    #check if main parent bone has any string in it's ROTF Parent custom property
                    if parentBonePConst.target.pose.bones[mainParentBoneN].get('ROTF Parent') != None:
                        print(parentBonePConst.target.pose.bones[mainParentBoneN]['ROTF Parent'])
                        #add all objects and parent bones related found in main parent bone's custom property
                        objectsBonesList = parentBonePConst.target.pose.bones[mainParentBoneN]['ROTF Parent'].split("?")
                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            parentBoneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(parentBoneN)
                            else:
                                objectParentBoneDictionary[objectN] = [parentBoneN]

                #if boneP's parent does not contain ".copy.parent.rig" it means it should contain at least ".parent.rig" since parents of bones containing "child.rig" have parents containing either ".parent.rig" or ".copy.parent.rig"
                else:
                    #check if main parent bone has any string in it's ROTF Parent custom property
                    if parentBoneP.get('ROTF Parent') != None:
                        #look into custom property for ROTF Parent containing string "objectName1|boneName1?objectName2|boneName2?...
                        objectsBonesList = parentBoneP['ROTF Parent'].split("?")
                        print("ROTF Parent = "+parentBoneP['ROTF Parent'])

                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            propParentBoneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(propParentBoneN)
                            else:
                                objectParentBoneDictionary[objectN] = [propParentBoneN]

                #since boneP contains "child.rig" we want to add it's parent to the objectParentBoneDictionary
                parentObjectN = parentBoneP.id_data.name

                if parentObjectN in objectParentBoneDictionary:
                    objectParentBoneDictionary[parentObjectN].append(parentBoneN)
                else:
                    objectParentBoneDictionary[parentObjectN] = [parentBoneN]
        
        #add objects from the library to selection and set them to pose mode in case they were not in pose mode
        for objectN in objectParentBoneDictionary:
            obj = bpy.data.objects[objectN]
            obj.select_set(True)
        
        #set currently selected objects to object mode so that they can all be set to pose mode without causing issues
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='POSE')

        print(objectParentBoneDictionary)
        return(objectParentBoneDictionary)

    def SelectRelatedBones (self, context, objectParentBoneDictionary):
        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        bonesToRemoveNDictionary = dict()
        for objectN in objectParentBoneDictionary:
            obj = bpy.data.objects[objectN]
            print("going through object : "+objectN)
            #show hidden bone layer 3 to access hidden rig bones
            obj.data.layers[2] = True

            parentBonesNList = objectParentBoneDictionary[objectN]
            parentBonesNList  = list(dict.fromkeys(parentBonesNList))
            
            #find child bones related to parent bones found previously
            for parentBoneN in parentBonesNList:
                print("going through bone : "+parentBoneN)

                #add .parent.rig bone to bonesToRemoveNDictionary with it's object name as key
                bonesToRemoveNDictionary[objectN] = [parentBoneN]

                childrenBonesP = obj.pose.bones[parentBoneN].children
                childrenBonesNList = []
                for childBoneP in childrenBonesP:
                    #add .child.rig. to childrenBonesNList
                    childrenBonesNList.append(childBoneP.name)
                    #add .child.rig bones to bonesToRemoveNDictionary
                    bonesToRemoveNDictionary[objectN].append(childBoneP.name)

                #select child bones for future baking
                for childBoneN in childrenBonesNList:
                    obj.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
                
                if ".copy.parent.rig" in parentBoneN:
                    pass
                else:
                    #adds parent bone to the baking bones selection
                    obj.data.bones[parentBoneN.replace(".parent.rig",".rig")].select = True

        #return affected rig bones to the main layer 2
        bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        return(bonesToRemoveNDictionary)

    def RemoveUnwantedBones (self, context, bonesToRemoveNDictionary):
        print("removing unwanted bones")

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        for objectN in bonesToRemoveNDictionary:

            obj = bpy.data.objects[objectN]

            for boneN in bonesToRemoveNDictionary[objectN]:                
                obj.data.bones[boneN].select = True
            
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()
        
        for objectN in bonesToRemoveNDictionary:
            StateUtility.SetEditMode()
            armature = bpy.data.objects[objectN].data
            for boneN in bonesToRemoveNDictionary[objectN]:
                armature.edit_bones.remove(armature.edit_bones[boneN])

    def OldRemoveParentSpace (self, context):
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #make list out of selection
        selectedBonesPList = bpy.context.selected_pose_bones.copy()
        selectedBonesPList.sort(key = lambda x:len(x.parent_recursive))

        #dictionary containing parent bones list using their object as key
        objectParentBoneDictionary = dict()

        #find parent bones with the suffix ".parent.rig" and add the to objectParentBoneDictionary with their respective object as key
        for boneP in selectedBonesPList:
            print("going through selected bone : " + boneP.name)
            #if boneP contains ".parent.rig"
            if ".parent.rig" in boneP.name:
                #if boneP contains "copy.parent.rig"
                if "copy.parent.rig" in boneP.name:
                    #if boneP's object is already used as key for the objectParentBoneDictionary
                    if boneP.id_data in objectParentBoneDictionary:
                        objectParentBoneDictionary[boneP.id_data.name].append(boneP.name)
                    #if boneP's object is not yet used as key for the objectParentBoneDictionary
                    else:
                        objectParentBoneDictionary[boneP.id_data.name] = [boneP.name]
                    #since boneP contains "copy.parent.rig" it should have a Copy Transform contraint
                    bonePConst = boneP.constraints['Copy Transforms']
                    #add object from boneP's constraint target to objectParentBoneDictionary key
                    if bonePConst.target in objectParentBoneDictionary:
                        objectParentBoneDictionary[bonePConst.target].append(bonePConst.subtarget)
                    else:
                        objectParentBoneDictionary[bonePConst.target] = [bonePConst.subtarget]
                #if boneP does not contain "copy.parent.rig" but still contains "parent.rig"
                else:
                    #add boneP to the objectParentBoneDictionary
                    if boneP.id_data in objectParentBoneDictionary:
                        objectParentBoneDictionary[boneP.id_data.name].append(boneP.name)
                    else:
                        objectParentBoneDictionary[boneP.id_data.name] = [boneP.name]
                    #since boneP is the main parent it should have a ROTF Parent custom property if not it's beceause the main parent affects only bones from the same object
                    if boneP.get('ROTF Parent') != None:
                        #look into custom property for ROTF Parent containing string "objectName1|boneName1?objectName2|boneName2?...
                        objectsBonesList = boneP['ROTF Parent'].split("?")
                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            boneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(boneN)
                            else:
                                objectParentBoneDictionary[objectN] = [boneN]

            #find parent bones through related .child
            if ".child.rig" in boneP.name:
                parentBoneN = boneP.parent.name
                parentBoneP = boneP.parent

                #if boneP's parent contains ".copy.parent.rig"
                if ".copy.parent.rig" in parentBoneN:
                    #find the copy transform constraint of the selected bones containing ".copy.parent.rig" in their name
                    parentBonePConst = boneP.parent.constraints['Copy Transforms']
                    #set aside the main parent's name to which all have been parented to previously
                    mainParentBoneN = parentBonePConst.subtarget

                    #add object from boneP's constraint target to objectParentBoneDictionary key
                    if parentBonePConst.target in objectParentBoneDictionary:
                        objectParentBoneDictionary[parentBonePConst.target.name].append(mainParentBoneN)
                    else:
                        objectParentBoneDictionary[parentBonePConst.target.name] = [mainParentBoneN]

                    #check if main parent bone has any string in it's ROTF Parent custom property
                    if parentBonePConst.target.pose.bones[mainParentBoneN].get('ROTF Parent') != None:
                        print(parentBonePConst.target.pose.bones[mainParentBoneN]['ROTF Parent'])
                        #add all objects and parent bones related found in main parent bone's custom property
                        objectsBonesList = parentBonePConst.target.pose.bones[mainParentBoneN]['ROTF Parent'].split("?")
                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            parentBoneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(parentBoneN)
                            else:
                                objectParentBoneDictionary[objectN] = [parentBoneN]

                #if boneP's parent does not contain ".copy.parent.rig" it means it should contain at least ".parent.rig" since parents of bones containing "child.rig" have parents containing either ".parent.rig" or ".copy.parent.rig"
                else:
                    #check if main parent bone has any string in it's ROTF Parent custom property
                    if parentBoneP.get('ROTF Parent') != None:
                        #look into custom property for ROTF Parent containing string "objectName1|boneName1?objectName2|boneName2?...
                        objectsBonesList = parentBoneP['ROTF Parent'].split("?")
                        print("ROTF Parent = "+parentBoneP['ROTF Parent'])

                        for objectBoneList in objectsBonesList:
                            objectBone = objectBoneList.split("|")
                            objectN = objectBone[0]
                            propParentBoneN = objectBone[1]
                            if objectN in objectParentBoneDictionary:
                                objectParentBoneDictionary[objectN].append(propParentBoneN)
                            else:
                                objectParentBoneDictionary[objectN] = [propParentBoneN]

                #since boneP contains "child.rig" we want to add it's parent to the objectParentBoneDictionary
                parentObjectN = parentBoneP.id_data.name

                if parentObjectN in objectParentBoneDictionary:
                    objectParentBoneDictionary[parentObjectN].append(parentBoneN)
                else:
                    objectParentBoneDictionary[parentObjectN] = [parentBoneN]

        print(objectParentBoneDictionary)

        #add objects from the library to selection and set them to pose mode in case they were not in pose mode
        for objectN in objectParentBoneDictionary:
            obj = bpy.data.objects[objectN]
            obj.select_set(True)
        
        #set currently selected objects to object mode so that they can all be set to pose mode without causing issues
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='POSE')

        #list of bones to select once the function ends
        endSelectedBonesP = list()

        #restore parent per object
        for objectN in objectParentBoneDictionary:
            obj = bpy.data.objects[objectN]
            print("going through object : "+objectN)

            parentBonesNList = objectParentBoneDictionary[objectN]
            parentBonesNList  = list(dict.fromkeys(parentBonesNList))
            
            #find child bones related to parent bones found previously
            for parentBoneN in parentBonesNList:
                print("going through bone : "+parentBoneN)

                #deselects all
                bpy.ops.pose.select_all(action='DESELECT')
                #list children of parentBone
                childrenBonesP = obj.pose.bones[parentBoneN].children
                childrenBonesN = []
                for childBoneP in childrenBonesP:
                    childrenBonesN.append(childBoneP.name)

                #show hidden bone layer 3 to access hidden rig bones
                obj.data.layers[2] = True

                #select child bones for future baking
                for childBoneN in childrenBonesN:
                    obj.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
                
                if ".copy.parent.rig" in parentBoneN:
                    pass
                else:
                    #adds parent bone to the baking bones selection
                    obj.data.bones[parentBoneN.replace(".parent.rig",".rig")].select = True

                #return affected rig bones to the main layer 2
                bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
                
                #bake animation on selection and remove constraints
                StateUtility.BakeAnimation()                

                #remove selected .parent.rig and related .child.rig bones
                bonesToRemove = []
                for boneN in childrenBonesN:
                    bonesToRemove.append(boneN)
                bonesToRemove.append(parentBoneN)

                #deselects all
                bpy.ops.pose.select_all(action='DESELECT')

                for boneN in bonesToRemove:
                    obj.data.bones[boneN].select = True
                #clear all key frames of selected bones
                bpy.ops.anim.keyframe_clear_v3d()

                
                StateUtility.SetEditMode()
                armature = obj.data
                for boneN in bonesToRemove:
                    armature.edit_bones.remove(armature.edit_bones[boneN])

                #force pose mode
                bpy.ops.object.mode_set(mode='POSE')

                #select child bones
                for childBoneN in childrenBonesN:
                    obj.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
                
                if ".copy.parent.rig" in parentBoneN:
                    pass
                else:
                    obj.data.bones[parentBoneN.replace(".parent.rig",".rig")].select = True

                for boneP in bpy.context.selected_pose_bones:
                    endSelectedBonesP.append(boneP)

                #hide bone layer 3
                obj.data.layers[2] = False
        
        print("bones to select after script is done :")
        print(endSelectedBonesP)
        for boneP in endSelectedBonesP:
            boneP.bone.select = True