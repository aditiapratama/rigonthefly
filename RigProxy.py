#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility
from . PolygonShapesUtility import PolygonShapes

class RigProxyUtils:
    def RigProxy (self, context):
        currentFrame = bpy.context.scene.frame_current #set aside current frame to come back to it at the end of the script

        bpy.context.scene.frame_current = 0 #set current frame to 0

        PolygonShapes.AddControllerShapes()

        targetObject = bpy.context.active_object # proxy rig object to copy

        RigProxyUtils.DuplicateProxyRig(targetObject)

        targetObject.hide_set(True)#hide proxy rig object

        #make target proxy rig bones' follow the new rig 
        for boneP in targetObject.pose.bones:
            print(boneP.name)
            copyTransforms = boneP.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.active_object
            copyTransforms.subtarget = boneP.name + ".rig"

        bpy.context.scene.frame_current = currentFrame #return to initial frame

    @staticmethod
    def DuplicateProxyRig(targetObject):        
        targetArmature = targetObject.data

        bpy.ops.object.mode_set(mode='OBJECT')
        #deselect all
        bpy.ops.object.select_all(action='DESELECT')

        targetBoneDictionary = dict() #to contain the bones info from the target proxy rig needed create a copy of it that is not linked/referenced

        #add bone info to copy to targetBoneDictionary
        for bone in targetArmature.bones:
            boneN = bone.name
            boneMatrix = bone.matrix_local #head position and roll in edit mode
            boneTail = bone.tail_local #tail position in edit mode
            #parent name
            if bone.parent == None:
                boneParentN = ""
            else:
                boneParentN = bone.parent.name

            targetBoneDictionary[boneN] = [boneMatrix, boneTail, boneParentN]

        newArmature = bpy.data.armatures.new(targetArmature.name + ".copy") #create new armature with the same name as tragetArmature but ends with ".copy"
        newObject = bpy.data.objects.new(targetObject.name + ".copy", newArmature) #create new object ending with ".copy" containing new armature

        bpy.context.collection.objects.link(bpy.data.objects[newObject.name]) #add newObject to collection so that it is visible in the scene #bpy.data.collections['Collection']
        bpy.context.view_layer.objects.active = bpy.data.objects[newObject.name] #set newObject to active

        #set to edit mode
        bpy.ops.object.mode_set(mode='EDIT')

        #create new bones
        for boneN in targetBoneDictionary:
            newBone = newArmature.edit_bones.new(boneN + ".rig")
            newBone.matrix = targetBoneDictionary[boneN][0] #head position and roll
            newBone.tail = targetBoneDictionary[boneN][1] #tail position
            newboneParentN = targetBoneDictionary[boneN][2] + ".rig" #parent name

            newBoneParent = newArmature.edit_bones.get(newboneParentN) #convert parent name to edit bone
            newBone.parent = newBoneParent #set a newBoneParent as parent of boneN

            #move bone to layer 2
            newBone.layers[1]=True
            newBone.layers[0]=False
        #show layer 2 and hide layer 1 of the armature
        newArmature.layers[1]=True
        newArmature.layers[0]=False

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #add pose bones groups to the newAmrature
        newObject.pose.bone_groups.new(name="Base")
        newObject.pose.bone_groups['Base'].color_set = 'THEME09'

        newObject.pose.bone_groups.new(name="Right")
        newObject.pose.bone_groups['Right'].color_set = 'THEME01'

        newObject.pose.bone_groups.new(name="Left")
        newObject.pose.bone_groups['Left'].color_set = 'THEME04'

        leftSide = ["left","_l","l_",".l","l.","-l","l-"," left","left "]
        rightSide = ["right","_r","r_",".r","r.","-r","r-"," right","right "]

        for boneP in newObject.pose.bones:
            boneP.custom_shape = bpy.data.objects["Circle"]
            bpy.context.object.data.bones[boneP.name].show_wire = True
            boneP.rotation_mode = 'YZX'

            if any(boneP.name.casefold().startswith(left) or boneP.name.casefold().endswith(left) for left in leftSide):
                boneP.bone_group_index = 2
            elif any(boneP.name.casefold().startswith(right) or boneP.name.casefold().endswith(right) for right in rightSide):
                boneP.bone_group_index = 1
            else:
                boneP.bone_group_index = 0
        
        #add a key frame on the whole rig to automatically add an action in case the rig has none
        bpy.ops.anim.keyframe_insert_menu(type='WholeCharacter')



