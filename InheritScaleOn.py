#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class InheritScaleOnUtils:

    def InheritScaleOn (self, context):
        
        selectedBonesListN = StateUtility.TempBoneCopySelectedBones()

        #inherit scale to original bone selection
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone].use_inherit_scale = True

        StateUtility.SelectedBonesCopyTempBones(selectedBonesListN)