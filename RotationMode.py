#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class RotationModeUtils:

    def RotationMode (self, context, rotationMode):

        selectedBonesListN = StateUtility.TempBoneCopySelectedBones()
                
        #change rotation mode of selected bones
        for bone in selectedBonesListN:
            bpy.context.object.pose.bones[bone].rotation_mode = rotationMode
            #bpy.context.object.pose.bones["l_Arm_ShoulderSHJnt.orient.rig"].rotation_mode = 'QUATERNION'

        StateUtility.SelectedBonesCopyTempBones(selectedBonesListN)