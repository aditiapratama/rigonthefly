#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . RigOnSkeleton import RigOnSkeletonUtils

class RigOnSkeletonOperator(bpy.types.Operator):
    """Really?"""
    bl_idname = bl_idname = "view3d.rig_on_skeleton_operator"
    bl_label = "Rename bones ending in .### to _###?"
    bl_description = "Adds basic FK rig controllers to skeleton, will cause issues if bones end in .###"
    bl_options = {'REGISTER', 'INTERNAL'}

    invalidBoneList = []

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        for bone in self.invalidBoneList:
            #fix bone name
            dotIndex = bone.name.rindex('.')
            bone.name = bone.name[:dotIndex] + '_' + bone.name[dotIndex+1:]
        
        RigOnSkeletonUtils.RigOnSkeleton(self, context)
        return {'FINISHED'}

    def invoke(self, context, event):
        validNames = True
        for bone in bpy.context.object.data.bones:
            if '.' not in bone.name :
                continue

            dotIndex = bone.name.rindex('.')
            if dotIndex == len(bone.name)-1 :
                continue

            substring = bone.name[dotIndex+1:len(bone.name)-1]
            validBoneName = False
            for character in substring:
                if not character.isdigit():
                    validBoneName = True
                    break
            
            if not validBoneName :
                self.invalidBoneList.append(bone)
                validNames = False

        if validNames :
            RigOnSkeletonUtils.RigOnSkeleton(self, context)
            return {'FINISHED'}
        return context.window_manager.invoke_confirm(self, event)