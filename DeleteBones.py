#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class DeleteBonesUtils:

    def DeleteBones (self, context):

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')
        
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #armature is in edit mode
        StateUtility.SetEditMode()

        #delete selected bones
        bpy.ops.armature.delete()

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')