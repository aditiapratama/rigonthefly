#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy

bpy.types.Object.aimDistance = bpy.props.FloatProperty(name= "aimDistance", default = 1, min=0)
bpy.types.Object.startFrame = bpy.props.IntProperty(name= "startFrame", default = 1)
bpy.types.Object.endFrame = bpy.props.IntProperty(name= "endFrame", default = 100)
bpy.types.Object.inertia = bpy.props.FloatProperty(name= "inertia", default = 0.2, min=0, max=1)
bpy.types.Object.smartBake = bpy.props.BoolProperty(name= "smartBake", description="When checked ON, any baking will result in keys only where they were previously, instead of keys every frame. Ideal for the basic posing phase.", default = False)

class RigOnTheFlyBase:
    bl_category ="Rig on the Fly"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

class RigOnTheFly(RigOnTheFlyBase, bpy.types.Panel):
    bl_idname = "RigOnTheFly"
    bl_label = "Rig on the Fly : v 0.9.2" #tab name
    
    def draw(self,context):
        layout = self.layout

class RigBake(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "1. Rig & Bake"

    def draw(self, context):
        layout = self.layout
        rowSizeY = 1.4

        row = layout.row(align=True)
        row.scale_y= rowSizeY
        row.operator('view3d.rig_on_skeleton_operator', text="Rig Skeleton", icon='ARMATURE_DATA')
        row.scale_y= rowSizeY
        row.operator('view3d.bake_on_skeleton_operator', text="Bake Rig", icon='OUTLINER_OB_ARMATURE')

        row = layout.row(align=True)
        row.scale_y= rowSizeY
        row.operator('view3d.auto_bone_orient_operator', text="Orient Rig", icon='ARMATURE_DATA')
        row.scale_y= rowSizeY
        row.operator('view3d.bake_orient_on_skeleton_operator', text="Bake Orient", icon='OUTLINER_OB_ARMATURE')

        row = layout.row(align=True)
        row.scale_y= rowSizeY
        row.operator('view3d.rig_proxy_operator', text="Rig Proxy", icon='ARMATURE_DATA')
        row.scale_y= rowSizeY
        row.operator('view3d.bake_proxy_operator', text="Bake Proxy", icon='OUTLINER_OB_ARMATURE')

class Settings(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "2. Settings"

    def draw(self, context):
        layout = self.layout
        obj = context.object

        col = layout.column(align=False)
        col.scale_y=1.5
        col.prop(obj,"smartBake", text="Smart Bake")

class ControllerSize(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "3. Controller Size"

    def draw(self, context):
        layout = self.layout

        row = layout.row(align=True)
        row.operator('view3d.controller_size_plus_operator', text="+", icon='ZOOM_IN')
        row.operator('view3d.controller_size_minus_operator', text="-", icon='ZOOM_OUT')

class IKFKSwitch(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "4. IK FK Switch"

    def draw(self, context):
        layout = self.layout
        
        row = layout.row(align=True)
        col = row.column(align=True)
        col.operator('view3d.ik_limb_operator', text="IK", icon='CON_KINEMATIC')
        col.operator('view3d.ik_limb_no_pole_operator', text="IK no P", icon='CON_KINEMATIC')        

        col = row.column(align=True)
        col.scale_y = 2.0
        row = layout.row()
        col.operator('view3d.fk_limb_operator', text="FK", icon='CON_ROTLIKE')        
               
        #row.operator('view3d.ik_pole_angle_operator', text="Shift IK Pole Angle", icon='CON_ROTLIMIT')

class RotationScaleTools(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "5. Rotation and Scale Tools"

    def draw(self, context):
        layout = self.layout
        layout.menu(RotationModeMenu.bl_idname, icon='ORIENTATION_GIMBAL')

        row = layout.row(align=True)
        row.operator('view3d.rotation_distribution_operator', text="Distribute", icon='STRANDS')
        row.operator('view3d.apply_distribution_operator', text="Apply", icon='MOD_THICKNESS')
	    
        row = layout.row(align=True)
        row.label(text="Inherit Rotation")
        row = row.row(align=True)
        row.scale_x=0.7
        row.operator('view3d.inherit_rotation_on_operator', text="On")
        row.operator('view3d.inherit_rotation_off_operator', text="Off")

        row = layout.row(align=True)
        row.label(text="Inherit Scale")
        row = row.row(align=True)
        row.scale_x=0.7
        row.operator('view3d.inherit_scale_on_operator', text="On")
        row.operator('view3d.inherit_scale_off_operator', text="Off")

class RotationModeMenu(bpy.types.Menu):
    bl_label = "Rotation Mode"
    bl_idname = "OBJECT_MT_custom_menu"

    def draw(self, context):
        layout = self.layout        
        
        layout.operator('view3d.rotation_mode_operator', text="Quaternion").rotationMode ='QUATERNION'
        layout.operator('view3d.rotation_mode_operator', text="XYZ").rotationMode = 'XYZ'
        layout.operator('view3d.rotation_mode_operator', text="XZY").rotationMode = 'XZY'
        layout.operator('view3d.rotation_mode_operator', text="YXZ").rotationMode = 'YXZ'
        layout.operator('view3d.rotation_mode_operator', text="YZX (default)").rotationMode = 'YZX'
        layout.operator('view3d.rotation_mode_operator', text="ZXY").rotationMode = 'ZXY'
        layout.operator('view3d.rotation_mode_operator', text="ZYX").rotationMode = 'ZYX'

class ExtraBone(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "6. Extra Controller"

    def draw(self, context):
        layout = self.layout

        row = layout.row(align=True)
        row.operator('view3d.add_extra_bone_operator', text="Add", icon='EMPTY_AXIS')
        row.operator('view3d.delete_bones_operator', text="Delete", icon='SORTBYEXT')

class SpaceSwitch(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "7. Space Switch"

    def draw(self, context):
        layout = self.layout
        obj = context.object
        
        row = layout.row(align=True)
        row.operator('view3d.world_position_operator', text="World Transforms", icon='ORIENTATION_GLOBAL')
        row.operator('view3d.remove_world_transforms_operator', text="Remove World", icon='OBJECT_ORIGIN')

        col = layout.column(align=True)
        row = col.row(align=True)
        row.operator('view3d.aim_world_operator', text= "World Aim", icon='CON_TRACKTO')
        row.operator('view3d.stretch_world_operator', text= "World Stretch", icon='CON_TRACKTO')
        col.prop(obj,"aimDistance", text="Distance")
        row = col.row(align=True)
        row.operator('view3d.aim_chain_operator', text="Aim Chain", icon='CON_CHILDOF')
        row.operator('view3d.stretch_chain_operator', text="Stretch Chain", icon='CON_CHILDOF')
        row = layout.row(align=True)
        row.operator('view3d.fk_space_operator', text="Remove Aim", icon='CON_SHRINKWRAP')

        col = layout.column(align=True)
        row = col.row(align=True)
        row.operator('view3d.parent_space_operator', text= "Parent", icon='PIVOT_ACTIVE')
        row.operator('view3d.parent_space_copy_operator', text= "Parent to Copy", icon='PIVOT_INDIVIDUAL')
        col.operator('view3d.remove_parent_space_operator', text= "Restore Parent", icon='PIVOT_MEDIAN')
        
class InertiaOnTransforms(RigOnTheFlyBase, bpy.types.Panel):
    bl_parent_id = "RigOnTheFly"
    bl_label = "8. Inertia On Transforms"

    def draw(self, context):
        layout = self.layout
        obj = context.object
        
        col = layout.column(align=True)

        row = col.row(align=True)
        row.prop(obj,"startFrame", text="Start")
        row.prop(obj,"endFrame", text="End")

        row= col.row(align=True)
        row.prop(obj,"inertia", text="Inertia Value", icon='IPO_ELASTIC')

        row = col.row(align=True)
        row.operator('view3d.translation_inertia_operator', text="Loc", icon='CON_LOCLIMIT')
        row.operator('view3d.rotation_inertia_operator', text="Rot", icon='CON_ROTLIMIT')
        row.operator('view3d.scale_inertia_operator', text= "Scale", icon='CON_SIZELIMIT')


