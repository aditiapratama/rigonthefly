#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class ParentSpaceUtils:

    @staticmethod
    def ParentSpaceCondition ():
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        for activeBoneParent in bpy.context.active_pose_bone.parent_recursive:
            for selectedBoneP in bpy.context.selected_pose_bones:
                if activeBoneParent == selectedBoneP:
                    return [{'WARNING'}, "target parent is child of selection"]

    @staticmethod
    def ParentDuplicateRename ():
        print("duplicating and renaming selection")
        #force edit mode in case it was not
        StateUtility.SetEditMode()

        #duplicate rig bones to be used as aim bones
        bpy.ops.armature.duplicate()

        #rename duplicated bones
        for boneE in bpy.context.selected_editable_bones:
            if boneE == bpy.context.active_bone:
                boneE.name = boneE.name.replace(".rig.001", ".parent.rig")
            else:
                boneE.name = boneE.name.replace(".rig.001", ".child.rig")

    @staticmethod
    def SortSelectionIntoDictionaries ():
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        print(bpy.context.selected_pose_bones)
        mainParentObjectBoneList = list()
        activeObjectChildrenNList = list()
        nonActiveObjectDictionary = dict()

        #go through selected pose bones
        for boneP in bpy.context.selected_pose_bones:

            #if boneP is part of the active object, add it to the activeObjectDictionary using object name as key
            if boneP.id_data == bpy.context.active_object:
                #if bone is the active bone, add it to the mainParentObjectBoneList
                if boneP == bpy.context.active_pose_bone:
                    mainParentObjectBoneList = [boneP.id_data.name, boneP.name]
                else:
                    activeObjectChildrenNList.append(boneP.name)
                    
            
            #if boneP is not part of the active object, add it to nonActiveObjectDictionary using object name as key
            else:
                if boneP.id_data.name in nonActiveObjectDictionary:
                    nonActiveObjectDictionary[boneP.id_data.name].append(boneP.name)
                else:
                    nonActiveObjectDictionary[boneP.id_data.name] = [boneP.name]

        print("main parent = ")
        print(mainParentObjectBoneList)
        print("active dictionary = ")
        print(activeObjectChildrenNList)
        print("non active dictionary = ")
        print(nonActiveObjectDictionary)
        return [activeObjectChildrenNList, nonActiveObjectDictionary, mainParentObjectBoneList]
    
    @staticmethod    
    def ParentActiveArmature (activeObjectChildrenNList, mainParentObjectBoneList):

        #force edit mode
        StateUtility.SetEditMode()

        parentObjectN = mainParentObjectBoneList[0]
        activeObject = bpy.data.objects[parentObjectN]
        activeArmature = activeObject.data
        print("active armature :")
        print(activeArmature)
        parentBoneN = mainParentObjectBoneList[1]

        #remove the active bone's parent
        parentBoneE = activeArmature.edit_bones[parentBoneN]
        #parentBoneE.parent = None

        for boneN in activeObjectChildrenNList:
            boneE = activeArmature.edit_bones[boneN]
            boneE.parent = parentBoneE

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
                
        print("active object : ")
        print(activeObjectChildrenNList)
        
        #set layer 3 to visible
        activeArmature.layers[2] = True

        #change child bones' display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        for boneN in activeObjectChildrenNList:
            bone = activeObject.pose.bones[boneN]
            bone.custom_shape = bpy.data.objects["Octagon"]
            bone.custom_shape_scale *= 1.5
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = activeObject
            copyTransforms.subtarget = boneN.replace(".child.rig",".rig")            

        #change parent bone's display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        parentBoneP = activeObject.pose.bones[parentBoneN]
        parentBoneP.custom_shape = bpy.data.objects["Octagon"]
        parentBoneP.custom_shape_scale *= 1.5
        copyTransforms = parentBoneP.constraints.new('COPY_TRANSFORMS')
        copyTransforms.target = activeObject
        copyTransforms.subtarget = parentBoneN.replace(".parent.rig",".rig")

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #initially selected bones follow .child.rig bones
        for childBoneN in activeObjectChildrenNList:
            rigBone = activeObject.pose.bones[childBoneN.replace(".child.rig",".rig")]
            copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = activeObject
            copyTransforms.subtarget = childBoneN
            #select initial rig bones to switch them to hidden layer 3 later
            activeArmature.bones[childBoneN.replace(".child.rig",".rig")].select = True

        #intially active bone follows .parent.rig bone
        rigBone = activeObject.pose.bones[parentBoneN.replace(".parent.rig",".rig")]
        copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
        copyTransforms.target = activeObject
        copyTransforms.subtarget = parentBoneN
        #select initial rig bones to switch them to hidden layer 3 later
        activeArmature.bones[parentBoneN.replace(".parent.rig",".rig")].select = True
        
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()
        
        bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
        
        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #set layer 3 to hidden
        activeArmature.layers[2] = False        
    
    @staticmethod
    def ParentCopyActiveArmature (activeObjectChildrenNList, mainParentObjectBoneList):

        #force edit mode
        StateUtility.SetEditMode()

        parentObjectN = mainParentObjectBoneList[0]
        activeObject = bpy.data.objects[parentObjectN]
        activeArmature = activeObject.data
        print("active armature :")
        print(activeArmature)
        parentBoneN = mainParentObjectBoneList[1]

        #remove the active bone's parent
        parentBoneE = activeArmature.edit_bones[parentBoneN]
        parentBoneE.parent = None

        for boneN in activeObjectChildrenNList:
            boneE = activeArmature.edit_bones[boneN]
            boneE.parent = parentBoneE

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
                
        print("active object : ")
        print(activeObjectChildrenNList)
        
        #set layer 3 to visible
        activeArmature.layers[2] = True

        #change child bones' display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        for boneN in activeObjectChildrenNList:
            bone = activeObject.pose.bones[boneN]
            bone.custom_shape = bpy.data.objects["Octagon"]
            bone.custom_shape_scale *= 1.5
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = activeObject
            copyTransforms.subtarget = boneN.replace(".child.rig",".rig")            

        #change parent bone's display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        parentBoneP = activeObject.pose.bones[parentBoneN]
        parentBoneP.custom_shape = bpy.data.objects["Octagon"]
        parentBoneP.custom_shape_scale *= 1.5
        copyTransforms = parentBoneP.constraints.new('COPY_TRANSFORMS')
        copyTransforms.target = activeObject
        copyTransforms.subtarget = parentBoneN.replace(".parent.rig",".rig")

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #initially selected bones follow child bones
        for childBoneN in activeObjectChildrenNList:
            rigBone = activeObject.pose.bones[childBoneN.replace(".child.rig",".rig")]
            copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = activeObject
            copyTransforms.subtarget = childBoneN
            #select initial rig bones to switch them to hidden layer 3 later
            activeArmature.bones[childBoneN.replace(".child.rig",".rig")].select = True
        
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()
        
        bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
        
        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #set layer 3 to hidden
        activeArmature.layers[2] = False        
    
    @staticmethod
    def ParentNonActiveArmature (nonActiveObjectDictionary, mainParentObjectBoneList):
        for childObjectN in nonActiveObjectDictionary:
            print("non active object : ")
            print(childObjectN)

            #force edit mode
            StateUtility.SetEditMode()

            #get all the data for the main parent
            mainObjectN = mainParentObjectBoneList[0]
            mainObject = bpy.data.objects[mainObjectN]
            mainArmature = mainObject.data
            mainParentBoneN = mainParentObjectBoneList[1]

            obj = bpy.data.objects[childObjectN]
            armature = obj.data
            print("armature :")
            print(armature)            
            childrenBoneNList = nonActiveObjectDictionary[childObjectN]

            #set layer 3 to visible
            armature.layers[2] = True

            #add a new bone that will act as parent for the children bones of non active armatures
            newParentBoneE = armature.edit_bones.new(mainParentBoneN.replace(".parent.rig",".copy.parent.rig"))
            newParentBoneE.length = mainArmature.edit_bones[mainParentBoneN].length
            newParentBoneN = newParentBoneE.name

            #go through the selected bones in the children armature
            for childBoneN in childrenBoneNList:
                childBoneE = armature.edit_bones[childBoneN]
                #change bone parent to active bone
                childBoneE.parent = newParentBoneE

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')

            #add custom property to parentBoneP to help point at .copy.parent.rig bones on other armatures when removing parent space
            mainParentBoneP = mainObject.pose.bones[mainParentBoneN]
            rna_ui = mainParentBoneP.get('_RNA_UI')
            if rna_ui is None:
                mainParentBoneP['_RNA_UI'] = {}
                rna_ui = mainParentBoneP['_RNA_UI']
            rotfParent = mainParentBoneP.get('ROTF Parent')
            if rotfParent is None:
                mainParentBoneP["ROTF Parent"] = armature.name +"|"+ newParentBoneN
            else:
                mainParentBoneP["ROTF Parent"] += "?"+ armature.name +"|"+ newParentBoneN

            #change child bones' display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones' animation.
            for childBoneN in childrenBoneNList:
                childBoneP = obj.pose.bones[childBoneN]
                childBoneP.custom_shape = bpy.data.objects["Octagon"]
                childBoneP.custom_shape_scale *= 1.5
                copyTransforms = childBoneP.constraints.new('COPY_TRANSFORMS')
                copyTransforms.target = obj
                copyTransforms.subtarget = childBoneN.replace(".child.rig",".rig")
                #select childBone for future baking
                obj.data.bones[childBoneN].select = True

            #change parent bone's display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bone's animation.
            newParentBoneP = obj.pose.bones[newParentBoneN]
            newParentBoneP.custom_shape = bpy.data.objects["Octagon"]
            newParentBoneP.custom_shape_scale *= 1.5
            copyTransforms = newParentBoneP.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = mainObject
            copyTransforms.subtarget = mainParentBoneN

            #bake animation on selection and remove constraints
            StateUtility.BakeAnimation()

            #deselects all
            bpy.ops.pose.select_all(action='DESELECT')

            #initially selected bones follow child bones
            for childBoneN in childrenBoneNList:
                rigBone = obj.pose.bones[childBoneN.replace(".child.rig",".rig")]
                copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
                copyTransforms.target = obj
                copyTransforms.subtarget = childBoneN
                #select initial rig bones switch to hidden layer 3
                obj.data.bones[rigBone.name].select = True

            #select newParentBone to switch to hidden layer 3
            obj.data.bones[newParentBoneN].select = True

            #clear all key frames of selected bones
            bpy.ops.anim.keyframe_clear_v3d()
            #moves initially selected bone to layer 3
            bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

            #deselects all
            bpy.ops.pose.select_all(action='DESELECT')
            #set layer 3 to visible to prevent issues with bpy.ops.armature.select_all(action='DESELECT')
            obj.data.layers[2] = False