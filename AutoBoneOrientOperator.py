#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . AutoBoneOrient import AutoBoneOrientUtils

class AutoBoneOrientOperator(bpy.types.Operator):
    """Really?"""
    bl_idname = bl_idname = "view3d.auto_bone_orient_operator"
    bl_label = "Rename bones ending in .### to _###?"
    bl_description = "Creates basic FK rig on skeleton. Fixing orientation issues. Ideal for rigs coming from other 3D softwares, will cause issues if bones end in .###"
    bl_options = {'REGISTER', 'INTERNAL'}

    invalidBoneList = []

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        for bone in self.invalidBoneList:
            #fix bone name
            dotIndex = bone.name.rindex('.')
            bone.name = bone.name[:dotIndex] + '_' + bone.name[dotIndex+1:]
        
        AutoBoneOrientUtils.find_correction_matrix(self, context)
        return {'FINISHED'}

    def invoke(self, context, event):
        self.invalidBoneList.clear()
        validNames = True
        for bone in bpy.context.object.data.bones:
            if '.' not in bone.name :
                continue

            dotIndex = bone.name.rindex('.')
            if dotIndex == len(bone.name)-1 :
                continue

            substring = bone.name[dotIndex+1:len(bone.name)-1]
            validBoneName = False
            for character in substring:
                if not character.isdigit():
                    validBoneName = True
                    break
            
            if not validBoneName :
                self.invalidBoneList.append(bone)
                validNames = False

        if validNames :
            AutoBoneOrientUtils.find_correction_matrix(self, context)
            return {'FINISHED'}
        return context.window_manager.invoke_confirm(self, event)