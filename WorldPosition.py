#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class WorldPositionUtils:

    def WorldPosition (self, context):
        #force edit mode
        StateUtility.SetEditMode()

        #list selected bones' names
        selectedBonesNames = list()
        for selectedBone in bpy.context.selected_editable_bones:
            selectedBonesNames.append(selectedBone.name)

        #duplicate base armature. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #add .rig suffix to duplicate bones now known as rig bones.
        for copiedBone in bpy.context.selected_editable_bones: 
            copiedBone.name = copiedBone.name.replace(".rig.001",".world.rig")

            #find the matrix coordinates of the armature object
            armatureMatrix = bpy.context.object.matrix_world
            #invert armature's matrix to find where global(0,0,0) is in relation to the armature's position/roation
            armatureMatrixInvert= armatureMatrix.copy()
            armatureMatrixInvert.invert()
            #set aim bone position to global (0,0,0) with axis following world's
            copiedBone.matrix = armatureMatrixInvert

        #list duplicated bones' names
        duplicatedBonesNames = list()
        for duplicatedBone in bpy.context.selected_editable_bones:
            duplicatedBonesNames.append(duplicatedBone.name)
            duplicatedBone.parent = None

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #
        for duplicatedBone in bpy.context.selected_pose_bones:
            duplicatedBone.custom_shape = bpy.data.objects["Square"]
            bpy.context.object.data.bones[duplicatedBone.name].show_wire = True

            copyTransform = duplicatedBone.constraints.new('COPY_TRANSFORMS')
            copyTransform.target = bpy.context.object
            copyTransform.subtarget = duplicatedBone.name.replace(".world.rig",".rig")
        
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()        

        bpy.ops.pose.select_all(action='DESELECT')

        #
        for selectedBoneName in selectedBonesNames:
            bpy.context.object.data.bones[selectedBoneName].select =True

            copyTransform = bpy.context.object.pose.bones[selectedBoneName].constraints.new('COPY_TRANSFORMS')
            copyTransform.target = bpy.context.object
            copyTransform.subtarget = selectedBoneName.replace(".rig",".world.rig")
        
        print(bpy.context.selected_pose_bones)

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        #
        for duplicatedBoneName in duplicatedBonesNames:
            bpy.context.object.data.bones[duplicatedBoneName].select =True