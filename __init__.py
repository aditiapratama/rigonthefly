###################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

bl_info = {
    "name" : "RigOnTheFly",
    "author" : "Dypsloom",
    "description" : "",
    "blender" : (2, 80, 0),
    "version" : (0, 9, 2),
    "location" : "View3D",
    "warning" : "",
    "category" : "Animation & Rig"
}

import bpy

from . AutoBoneOrientOperator import AutoBoneOrientOperator
from . RigOnSkeletonOperator import RigOnSkeletonOperator
from . RigProxyOperator import RigProxyOperator
from . BakeProxyOperator import BakeProxyOperator
from . BakeOnSkeletonOperator import BakeOnSkeletonOperator
from . BakeOrientOnSkeletonOperator import BakeOrientOnSkeletonOperator
from . ControllerSizePlusOperator import ControllerSizePlusOperator
from . ControllerSizeMinusOperator import ControllerSizeMinusOperator
from . IKLimbOperator import IKLimbOperator
from . IKLimbNoPoleOperator import IKLimbNoPoleOperator
from . IKLimbPoleAngleOperator import IKLimbPoleAngleOperator
from . FKLimbOperator import FKLimbOperator
from . RotationModeOperator import RotationModeOperator
from . InheritRotationOffOperator import InheritRotationOffOperator
from . InheritRotationOnOperator import InheritRotationOnOperator
from . InheritScaleOffOperator import InheritScaleOffOperator
from . InheritScaleOnOperator import InheritScaleOnOperator
from . RotationDistributionOperator import RotationDistributionOperator
from . ApplyDistributionOperator import ApplyDistributionOperator
from . GroupSectionOperator import GroupSectionOperator
from . WorldPositionOperator import WorldPositionOperator
from . RemoveWorldTransformsOperator import RemoveWorldTransformsOperator
from . IKChainOperator import IKChainOperator
from . AimWorldOperator import AimWorldOperator
from . StretchWorldOperator import StretchWorldOperator
from . AimChainOperator import AimChainOperator
from . StretchChainOperator import StretchChainOperator
from . FKSpaceOperator import FKSpaceOperator
from . ParentSpaceOperator import ParentSpaceOperator
from . ParentSpaceCopyOperator import ParentSpaceCopyOperator
from . RemoveParentSpaceOperator import RemoveParentSpaceOperator
from . AddExtraBoneOperator import AddExtraBoneOperator
from . DeleteBonesOperator import DeleteBonesOperator
from . TranslationInertiaOperator import TranslationInertiaOperator
from . RotationInertiaOperator import RotationInertiaOperator
from . ScaleInertiaOperator import ScaleInertiaOperator
from . RigOnTheFly import RigOnTheFly, RigBake, Settings, ControllerSize, IKFKSwitch, RotationScaleTools, ExtraBone, SpaceSwitch, InertiaOnTransforms, RotationModeMenu

classes = (
    AutoBoneOrientOperator,
    RigOnSkeletonOperator, 
    RigProxyOperator,
    BakeProxyOperator,
    BakeOnSkeletonOperator,
    BakeOrientOnSkeletonOperator,
    ControllerSizePlusOperator, 
    ControllerSizeMinusOperator, 
    IKLimbOperator, 
    IKLimbNoPoleOperator,
    IKLimbPoleAngleOperator, 
    FKLimbOperator, 
    RotationModeOperator,
    InheritRotationOffOperator, 
    InheritScaleOffOperator, 
    InheritRotationOnOperator, 
    InheritScaleOnOperator,
    RotationDistributionOperator,
    ApplyDistributionOperator,
    GroupSectionOperator, 
    WorldPositionOperator,
    RemoveWorldTransformsOperator,
    IKChainOperator,
    AimWorldOperator,
    StretchWorldOperator,
    AimChainOperator,
    StretchChainOperator,   
    FKSpaceOperator, 
    ParentSpaceOperator, 
    ParentSpaceCopyOperator,
    RemoveParentSpaceOperator, 
    AddExtraBoneOperator, 
    DeleteBonesOperator,
    TranslationInertiaOperator, 
    RotationInertiaOperator, 
    ScaleInertiaOperator, 
    RigOnTheFly,
    RigBake,
    Settings,
    ControllerSize,
    IKFKSwitch,
    RotationScaleTools,
    ExtraBone,
    SpaceSwitch,
    InertiaOnTransforms,
    RotationModeMenu

)

register, unregister = bpy.utils.register_classes_factory(classes)