#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . RigProxy import RigProxyUtils

class RigProxyOperator(bpy.types.Operator):
    bl_idname = "view3d.rig_proxy_operator"
    bl_label = "Simple operator"
    bl_description = "Creates a control rig on proxy rig"

    def execute(self, context):
        RigProxyUtils.RigProxy(self, context)
        return {'FINISHED'}