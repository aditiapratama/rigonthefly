#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . Utility import StateUtility

class InheritScaleOffUtils:

    def InheritScaleOff (self, context):
        
        selectedBonesListN = StateUtility.TempBoneCopySelectedBones()

        #remove inherit scale to original bone selection
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone].use_inherit_scale = False

        StateUtility.SelectedBonesCopyTempBones(selectedBonesListN)