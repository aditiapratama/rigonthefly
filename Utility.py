#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from mathutils import Matrix, Euler, Vector
from typing import NamedTuple
from bpy_extras.io_utils import axis_conversion

class StateUtility:

    @staticmethod
    def SetEditMode (toggleMirror=False):
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.context.object.data.use_mirror_x = toggleMirror


    @staticmethod
    def SaveState ():
        mode = bpy.context.mode
        selectionEditableBones = bpy.context.selected_editable_bones
        selectionPoseBones = bpy.context.selected_pose_bones
        return StateData(mode,selectionEditableBones,selectionPoseBones)

    @staticmethod
    def RecoverState (stateData):
        bpy.ops.object.mode_set(mode= stateData.mode)
    
    @staticmethod
    def RemoveEditableBone(boneToRemove):
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in armature.edit_bones:
            if bone.name == boneToRemove.name:
                armature.edit_bones.remove(bone)

    @staticmethod
    def BoneListToNameList(boneList):
        nameList = []
        for b in boneList:
            nameList.append(b.name)
        return nameList       

    @staticmethod
    def TempBoneCopySelectedBones():
        #force edit mode
        StateUtility.SetEditMode()

        #list selected bones in edit mode
        selectedBonesListE = bpy.context.selected_editable_bones.copy()
        selectedBonesListE.sort(key = lambda x:len(x.parent_recursive))
        
        #list selected bones' names
        selectedBonesListN = []
        for b in selectedBonesListE:
            selectedBonesListN.append(b.name)
        
        #duplicate base armature. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        #add copy transform constrain to duplicated bones
        for bone in bpy.context.selected_pose_bones:
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = bone.name.replace(".rig.001",".rig")

        StateUtility.BakeAnimation()
        
        return selectedBonesListN

    @staticmethod
    def SelectedBonesCopyTempBones(selectedBonesListN):
        #deselect copied bones
        bpy.ops.pose.select_all(action='DESELECT')

        #select original bone list
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone].select = True

        #make selected bones follow duplicated bones with copy transform constraint
        for bone in bpy.context.selected_pose_bones:
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = bone.name.replace(".rig",".rig.001")

        StateUtility.BakeAnimation()

        #deselect all to
        bpy.ops.pose.select_all(action='DESELECT')
        #select bones to remove to remove their keyframes first
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone.replace(".rig",".rig.001")].select = True

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #remove copied bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in selectedBonesListN:
            armature.edit_bones.remove(armature.edit_bones[bone +".001"])
        
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        for rigBone in selectedBonesListN:
            bpy.context.object.data.bones[rigBone].select = True

        return selectedBonesListN

    @staticmethod
    def BakeAnimationWithOptions(bakeOptions):

        #bake copied bones animation so that they have the same animation as the base armature.
        bpy.ops.nla.bake(
            frame_start=bakeOptions.frame_start, 
            frame_end=bakeOptions.frame_end, 
            only_selected=bakeOptions.only_selected, 
            visual_keying=bakeOptions.visual_keying, 
            clear_constraints=bakeOptions.clear_constraints,
            clear_parents=bakeOptions.clear_parents,
            use_current_action=bakeOptions.use_current_action, 
            bake_types={'POSE'}
        )

    @staticmethod
    def BakeAnimation():
        if bpy.context.object.smartBake == True:
            StateUtility.SmartBake()

        else:
            bakeOptions=BakeOptions()
            
            bakeOptions.frame_start=bpy.context.active_object.animation_data.action.frame_range.x
            bakeOptions.frame_end=bpy.context.active_object.animation_data.action.frame_range.y

            StateUtility.BakeAnimationWithOptions(bakeOptions)

    @staticmethod
    def SmartBake():
        print("---------Smart Baking-----------")
        currentFrame = bpy.context.scene.frame_current
        selectedObjectBonesNDictionary=dict()
        relevantKeyedBonesNDictionary=dict()
        framesToKey=[0]
        #list object names to know how many objects have bone that will be baked
        objectNList = list()

        #add selected pose bones to selectedObjectBoneNDictionary with their respective object as key
        for boneP in bpy.context.selected_pose_bones:
            if boneP.id_data.name in selectedObjectBonesNDictionary:
                selectedObjectBonesNDictionary[boneP.id_data.name].append(boneP.name)
            else:
                selectedObjectBonesNDictionary[boneP.id_data.name] = [boneP.name]
        #go through bones in selectedObjectBoneNDictionary to find relevantKeyedBonesNDictionary
        for objectN in selectedObjectBonesNDictionary:
            obj=bpy.data.objects[objectN]

            for boneN in selectedObjectBonesNDictionary[objectN]:
                objectNList.append(objectN)
                try:
                    if obj.name in relevantKeyedBonesNDictionary:
                        relevantKeyedBonesNDictionary[obj.name].append(obj.pose.bones[boneN].constraints[0].subtarget)
                    else:
                        relevantKeyedBonesNDictionary[obj.name] = [obj.pose.bones[boneN].constraints[0].subtarget]
                except:
                    print("no subtarget")
                try:
                    if obj.name in relevantKeyedBonesNDictionary:
                        relevantKeyedBonesNDictionary[obj.name].append(obj.pose.bones[boneN].constraints[0].pole_subtarget)
                    else:
                        relevantKeyedBonesNDictionary[obj.name] = obj.pose.bones[boneN].constraints[0].pole_subtarget
                except:
                    print("no pole subtarget")

                parentBoneN = boneN.replace(".rig", ".parent.rig")
                if obj.pose.bones.get(parentBoneN):
                    try:
                        relevantKeyedBonesNDictionary[obj.name].append(parentBoneN)
                    except:
                        pass
        print("selectedObjectBonesN")
        print(selectedObjectBonesNDictionary)
        print("relevantKeyedBones:")
        print(relevantKeyedBonesNDictionary)

        #go through relevantKeyedBonesNDictionary to remove empty strings and duplicate bones names
        for objectN in relevantKeyedBonesNDictionary:
            objectNList.append(objectN)
            relevantKeyedBonesNList = relevantKeyedBonesNDictionary[objectN]
            print("relevant keyed bones list copy:")
            print(relevantKeyedBonesNList)
            #removes empty strings from list
            relevantKeyedBonesNList = list(filter(None, relevantKeyedBonesNList))
            #removes duplicates from list
            relevantKeyedBonesNList = list( dict.fromkeys(relevantKeyedBonesNList))

            print("relevant keyed bones list copy:")
            print(relevantKeyedBonesNList)

            relevantKeyedBonesNDictionary[objectN] = relevantKeyedBonesNList
        print("relevantKeyedBones:")
        print(relevantKeyedBonesNDictionary)

        #removes empty strings from list
        objectNList = list(filter(None, objectNList))
        #removes duplicates from list
        objectNList = list( dict.fromkeys(objectNList))

        print("object name list:")
        print(objectNList)

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #select all relevant keyed bones
        for objectN in relevantKeyedBonesNDictionary:
            for boneN in relevantKeyedBonesNDictionary[objectN]:
                print("boneN = " + boneN)
                bpy.data.objects[objectN].data.bones[boneN].select = True
        print("selected pose bones:")
        print(bpy.context.selected_pose_bones)

        #set current frame to the start of the animation
        bpy.context.scene.frame_current = 0

        #set aside current active object to return to it later
        activeObject = bpy.context.active_object
        #loop over each object in relevantKeyedBonesNDictionary to find the frames to key
        for objectN in objectNList:
            obj = bpy.data.objects[objectN]
            bpy.context.view_layer.objects.active = obj

            result={'FINISHED'}
            #find frames to key by jumping to next keyframe until it reaches the end
            while 'FINISHED' in result:
                result=bpy.ops.screen.keyframe_jump()
                framesToKey.append(bpy.context.scene.frame_current)
        #return to initial active object
        bpy.context.view_layer.objects.active = activeObject
        print("frames to key:")
        print(framesToKey)
        
        #if there are bones to bake on more than one object,
        if len(bpy.context.selected_objects) > 1 :
            #force object mode
            bpy.ops.object.mode_set(mode='OBJECT')
            
            print("keying objects")
            for frame in framesToKey:
                bpy.context.scene.frame_set(frame)
                #print(bpy.context.scene.frame_current)

                #key location of selected objects so that visual keying on bones will work as expected
                bpy.ops.anim.keyframe_insert_menu(type='Location')

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        for objectN in selectedObjectBonesNDictionary:
            #select all relevant keyed bones. 
            for boneN in selectedObjectBonesNDictionary[objectN]:
                bpy.data.objects[objectN].data.bones[boneN].select = True

        print("keying bones")
        #going through frames to key
        for frame in framesToKey:
            bpy.context.scene.frame_set(frame)
            #print(bpy.context.scene.frame_current)
            
            #key location rotation and scale taking into account contstraints
            bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_VisualLocRotScale')

        for objectN in selectedObjectBonesNDictionary:
            #remove constraints of selectedBones
            for boneN in selectedObjectBonesNDictionary[objectN]:
                constraints = bpy.data.objects[objectN].pose.bones[boneN].constraints
                for c in constraints:
                    constraints.remove( c )        

        bpy.context.scene.frame_current = currentFrame
"""
        #delete animations on object
        for ob in bpy.context.selected_objects:
            #print("removing animation on object: "+objectN)
            #ob = bpy.data.objects[objectN]
            print(ob.name)
            ad = ob.animation_data

            if ad:
                action = ad.action
                if action:
                    remove_types = ["location"]
                    # select all that have datapath above
                    fcurves = [fc for fc in action.fcurves
                            for type in remove_types
                            if fc.data_path.startswith(type)
                            ]
                    # remove fcurves
                    while(fcurves):
                        fc = fcurves.pop()
                        action.fcurves.remove(fc)
"""
class BakeOptions():
    mode:str
    frame_start=0
    frame_end=1
    only_selected=True
    visual_keying=True
    clear_parents=False
    clear_constraints=True

    use_current_action=True
    bake_types={'POSE'}

class StateData(NamedTuple):
    mode:str
    selectionEditableBones:bpy.types.EditBone
    selectionPoseBones:bpy.types.PoseBone