#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . IKLimbNoPole import IKLimbNoPoleUtils

class IKLimbNoPoleOperator(bpy.types.Operator):
    bl_idname = "view3d.ik_limb_no_pole_operator"
    bl_label = "Simple operator"
    bl_description = "Changes selected controller and it's two parents to work in IK woth no pole vector"

    def execute(self, context):
        IKLimbNoPoleUtils.IKLimbNoPole(self, context)
        return {'FINISHED'}