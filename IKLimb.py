#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
import math
from math import degrees
from . Utility import StateUtility

class IKLimbUtils:

    def PolePosition (self, context, baseBoneN, poleBoneN, targetBoneN, ikPoleBoneN, tempIKPolePointerN):
        ########################################
        # Create and place IK pole target bone #
        #          by Marco Giordano           #
        ########################################

        # Get points to define the plane on which to put the pole target
        objectE = bpy.context.active_object.data.edit_bones
        A = objectE[baseBoneN].head
        B = objectE[poleBoneN].head
        C = objectE[targetBoneN].head

        # Vector of chain root (shoulder) to chain tip (wrist)
        AC = C - A

        # Vector of chain root (shoulder) to second bone's head (elbow)
        AB = B - A

        # Multiply the two vectors to get the dot product
        dot_prod = AB @ AC

        # Find the point on the vector AC projected from point B
        proj = dot_prod / AC.length

        # Normalize AC vector to keep it a reasonable magnitude
        start_end_norm = AC.normalized()

        # Project an arrow from AC projection point to point B
        proj_vec  = start_end_norm * proj
        arrow_vec = AB - proj_vec
        arrow_vec = arrow_vec.normalized()

        # Place pole target at a reasonable distance from the chain
        arrow_vec *= AC.length/2
        final_vec = arrow_vec + B

        # place pole target bone in the scene pointed to Z+        
        objectE[ikPoleBoneN].head = final_vec
        objectE[ikPoleBoneN].tail = final_vec + arrow_vec
        objectE[ikPoleBoneN].length = AC.length *0.15
        objectE[ikPoleBoneN].roll = 0.0

        objectE[tempIKPolePointerN].head = B
        objectE[tempIKPolePointerN].tail = final_vec
        objectE[tempIKPolePointerN].roll = 0

        
    ##############
    # Pole Angle #
    # by Jerryno #
    ##############

    def signed_angle (self, context, vector_u, vector_v, normal):
        # Normal specifies orientation
        angle = vector_u.angle(vector_v)
        if vector_u.cross(vector_v).angle(normal) < 1:
            angle = -angle
        return angle

    def get_pole_angle (self, context, base_bone, poleBone, pole_location):
        print(poleBone.name + "pole bone")
        print(base_bone.name + "base bone")
        pole_normal = (poleBone.tail - base_bone.head).cross(pole_location - base_bone.head)
        projected_pole_axis = pole_normal.cross(base_bone.tail - base_bone.head)
        return IKLimbUtils.signed_angle(self, context, base_bone.x_axis, projected_pole_axis, base_bone.tail - base_bone.head)

    def GetNearestRightAngle (self, context, currentAngle):
            pole_angle0 = 0 #0°
            pole_angle90 = math.pi/2 #90°
            pole_angle180 = math.pi #180°
            pole_angle270 = -math.pi/2 #-90°

            if currentAngle < pole_angle0 + math.pi/4 and currentAngle > pole_angle0 - math.pi/4:
                return pole_angle0

            if currentAngle < pole_angle90 + math.pi/4 and currentAngle > pole_angle90 - math.pi/4:
                return pole_angle90

            if abs(currentAngle) > pole_angle180 - math.pi/4:
                return pole_angle180

            if currentAngle < pole_angle270 + math.pi/4 and currentAngle > pole_angle270 - math.pi/4:
                return pole_angle270

            return pole_angle0

    def PoleAngleRadian (self, context, base_bone, ik_bone, tempPole_bone, poleBone):
        pole_angle_in_radians = IKLimbUtils.get_pole_angle(
            self, 
            context, 
            base_bone,
            poleBone,
            tempPole_bone.matrix.translation)
        return (pole_angle_in_radians)
    
    def IKLimb (self, context):
        obj = bpy.context.active_object
        armature = obj.data
        #set aside current frame to come back to it at the end of the script
        currentFrame = bpy.context.scene.frame_current
        #set current frame to 0 (where the rig should be in bind pose)
        bpy.context.scene.frame_current = 0

        #add bone name to selectedBonesN to have it's generated IK controller selected at the end of the script
        selectedBonesN = list()
        for bone in bpy.context.selected_pose_bones:
            selectedBonesN.append(bone.name)

        for targetBoneN in selectedBonesN:
            #force edit mode
            StateUtility.SetEditMode()

            #get the parent and parent's parent of one of the selected bones in pose mode
            poleBoneP = bpy.context.object.pose.bones[targetBoneN].parent
            baseBoneP = bpy.context.object.pose.bones[targetBoneN].parent.parent
            #get the name of the parent and parent's parent of one of the selected bones
            poleBoneN = poleBoneP.name
            baseBoneN = baseBoneP.name

            #deselect all
            bpy.ops.armature.select_all(action='DESELECT')

            #selects and duplicates the last bone in the hierarchy of the original selection
            bpy.context.object.data.edit_bones[targetBoneN].select=True
            bpy.context.object.data.edit_bones[targetBoneN].select_head=True
            bpy.context.object.data.edit_bones[targetBoneN].select_tail=True
            bpy.ops.armature.duplicate()
            #rename ik bone
            bpy.context.object.data.edit_bones[targetBoneN +".001"].name = targetBoneN.replace(".rig",".IK.rig")

            ikTargetBoneN = targetBoneN.replace(".rig",".IK.rig")
            #remove parent
            bpy.context.selected_editable_bones[0].parent = None

            bpy.ops.armature.select_all(action='DESELECT')
            #selects and duplicates the second bone in the hierarchy of the original selection
            bpy.context.object.data.edit_bones[poleBoneN].select=True
            bpy.context.object.data.edit_bones[poleBoneN].select_head=True
            bpy.context.object.data.edit_bones[poleBoneN].select_tail=True

            #duplicate once for the temporary bone that will be inbetween the base bone and the target bone pointing at the pole bone
            bpy.ops.armature.duplicate()
            #rename ik bone            
            bpy.context.selected_editable_bones[0].name = bpy.context.selected_editable_bones[0].name.replace(".rig.001",".temp.Pointer.rig")
            tempIKPolePointerN = bpy.context.selected_editable_bones[0].name
            #remove parent
            armature.edit_bones[tempIKPolePointerN].parent = None

            #duplicate a second time for the bone placed at the wanted pole vector location
            bpy.ops.armature.duplicate()
            #rename ik bone            
            bpy.context.selected_editable_bones[0].name = bpy.context.selected_editable_bones[0].name.replace(".temp.Pointer.rig.001",".temp.Pole.rig")
            tempIKPoleBoneN = bpy.context.selected_editable_bones[0].name
            #set parent to tempIKPolePointerN
            armature.edit_bones[tempIKPoleBoneN].parent = armature.edit_bones[tempIKPolePointerN]

            #place temp ik pole correctly in edit mode
            IKLimbUtils.PolePosition(self, context, baseBoneN, poleBoneN, targetBoneN, tempIKPoleBoneN, tempIKPolePointerN)
            
            #duplicate ikPoleBone to get tempIKPoleBoneN used to get the desired motion for the ikPole
            bpy.ops.armature.duplicate()
            bpy.context.selected_editable_bones[0].name = bpy.context.selected_editable_bones[0].name.replace(".temp.Pole.rig.001",".Pole.rig")
            ikPoleBoneN = bpy.context.selected_editable_bones[0].name
            #remove parent
            armature.edit_bones[ikPoleBoneN].parent = None            

            #snap tail of selectedPoleBoneN to ikTargetBoneN head's position
            bpy.context.object.data.edit_bones[poleBoneN].tail = bpy.context.object.data.edit_bones[ikTargetBoneN].head

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')
            #change rig bones' display to square, rotation mode to euler YZX and adds copy transform constraint to copy the base armature's animation.
            ikTargetBoneP = bpy.context.object.pose.bones[ikTargetBoneN]
            ikTargetBoneP.custom_shape = bpy.data.objects["Square"]
            bpy.context.object.data.bones[ikTargetBoneP.name].show_wire = True
            ikTargetBoneP.rotation_mode = 'YZX'
            copyTransforms = ikTargetBoneP.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = ikTargetBoneP.name.replace(".IK.rig",".rig")

            #change rig bones' display to square, rotation mode to euler YZX and adds copy transform constraint to copy the base armature's animation.
            ikPoleBoneP = bpy.context.object.pose.bones[ikPoleBoneN]
            ikPoleBoneP.custom_shape = bpy.data.objects["Locator"]
            bpy.context.object.data.bones[ikPoleBoneP.name].show_wire = True
            ikPoleBoneP.rotation_mode = 'YZX'
            copyTransforms = ikPoleBoneP.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = ikPoleBoneP.name.replace(".Pole.rig",".temp.Pole.rig")

            #set asside tempIKPoleBoneP to calculate the IK's pole angle
            tempIKPoleBoneP = bpy.context.object.pose.bones[tempIKPoleBoneN]

            #constrain tempIKPolePointer to stay between the base bone and the target bone pointing towards the pole bone
            tempIKPolePointerP = bpy.context.object.pose.bones[tempIKPolePointerN]
            #copy location of base bone
            copyLocation = tempIKPolePointerP.constraints.new('COPY_LOCATION')
            copyLocation.target = bpy.context.object
            copyLocation.subtarget = baseBoneN
            #copy 0.5* location of target bone
            copyLocation = tempIKPolePointerP.constraints.new('COPY_LOCATION')
            copyLocation.target = bpy.context.object
            copyLocation.subtarget = targetBoneN
            copyLocation.influence = 0.5
            #point towards pole bone
            pointerIK = tempIKPolePointerP.constraints.new('IK')
            pointerIK.target = bpy.context.object
            pointerIK.subtarget = poleBoneN

            
            #only adds ikTargetBoneN to selection since ikPoleBone is already selected
            bpy.context.object.data.bones[tempIKPoleBoneN].select = True
            bpy.context.object.data.bones[ikTargetBoneN].select = True
            #remove tempIKPoleBoneN from selection to prevent baking it
            #bpy.context.object.data.bones[tempIKPoleBoneN].select = False

            print(bpy.context.selected_pose_bones)
            #bake animation on selection and remove constraints
            StateUtility.BakeAnimation()

            #adds ik constraint to selectedPoleBoneN
            ikBone = bpy.context.object.pose.bones[poleBoneN]
            ik = ikBone.constraints.new('IK')
            ik.target = bpy.context.object
            ik.subtarget = ikTargetBoneN
            ik.pole_target = bpy.context.object
            ik.pole_subtarget = ikPoleBoneN
            ik.pole_angle = -math.pi/2 #-90°
            ik.pole_angle = IKLimbUtils.PoleAngleRadian (self, context, baseBoneP, ikTargetBoneP, tempIKPoleBoneP, poleBoneP)
            ik.chain_count = 2

            #selectedTargetBone follow ikTargetBone transforms
            selectedTargetBone = bpy.context.object.pose.bones[targetBoneN]
            copyTransforms = selectedTargetBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = selectedTargetBone.name.replace(".rig",".IK.rig")

            #deselect all to prevent baking bones that were left selected
            bpy.ops.pose.select_all(action='DESELECT')

            bpy.context.object.data.bones[targetBoneN].select = True
            bpy.context.object.data.bones[poleBoneN].select = True
            bpy.context.object.data.bones[baseBoneN].select = True            

            #clear all key frames of selected bones
            bpy.ops.anim.keyframe_clear_v3d()

            #deselect all to prevent baking bones that were left selected
            bpy.ops.pose.select_all(action='DESELECT')

            #remove tempIKPoleBoneN
            StateUtility.SetEditMode()
            armature = bpy.context.object.data
            try:
                armature.edit_bones.remove(armature.edit_bones[tempIKPoleBoneN])
                armature.edit_bones.remove(armature.edit_bones[tempIKPolePointerN])
            except:
                print(tempIKPoleBoneN)
            #return to pose mode
            bpy.ops.object.mode_set(mode='POSE')

            #move non relevant bones to layer 3
            bpy.context.object.data.bones[targetBoneN].layers[2]=True
            bpy.context.object.data.bones[targetBoneN].layers[1]=False

            bpy.context.object.data.bones[poleBoneN].layers[2]=True
            bpy.context.object.data.bones[poleBoneN].layers[1]=False

            bpy.context.object.data.bones[baseBoneN].layers[2]=True
            bpy.context.object.data.bones[baseBoneN].layers[1]=False

        #end script with new ik handles selected
        for targetBoneN in selectedBonesN:
            bpy.context.object.data.bones[targetBoneN.replace(".rig",".IK.rig")].select = True

        #reset current frame to where it was initially
        bpy.context.scene.frame_current = currentFrame
