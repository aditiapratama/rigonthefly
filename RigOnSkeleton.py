#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . PolygonShapesUtility import PolygonShapes
from . Utility import StateUtility

class RigOnSkeletonUtils:
    
    def RigOnSkeleton (self, context):
        #set aside current frame to come back to it at the end of the script
        currentFrame = bpy.context.scene.frame_current

        #add controller shapes
        PolygonShapes.AddControllerShapes()

        #set aside the armature as a variable
        armature = bpy.context.object

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')        

        #select base armature
        bpy.ops.pose.select_all(action='SELECT')

        #go to frame 0 and key the rig in bind/rest pose
        bpy.context.scene.frame_set(0)
        for orientBone in bpy.context.selected_pose_bones:
            orientBone.location = (0,0,0)
            orientBone.rotation_quaternion = (1,0,0,0)
            orientBone.scale = (1,1,1)

        #add a key frame on the whole rig to automatically add an action in case the rig has none
        bpy.ops.anim.keyframe_insert_menu(type='WholeCharacter')
        
        if armature.pose.bone_groups.get('Base') is None:
            armature.pose.bone_groups.new(name="Base")
            armature.pose.bone_groups['Base'].color_set = 'THEME09'

        if armature.pose.bone_groups.get('Right') is None:
            armature.pose.bone_groups.new(name="Right")
            armature.pose.bone_groups['Right'].color_set = 'THEME01'

        if armature.pose.bone_groups.get('Left') is None:
            armature.pose.bone_groups.new(name="Left")
            armature.pose.bone_groups['Left'].color_set = 'THEME04'

        leftSide = ["left","_l","l_",".l","l.","-l","l-"," left","left "]
        rightSide = ["right","_r","r_",".r","r.","-r","r-"," right","right "]

        for poseBone in bpy.context.selected_pose_bones:
            if any(poseBone.name.casefold().startswith(left) or poseBone.name.casefold().endswith(left) for left in leftSide):
                poseBone.bone_group_index = 2
            elif any(poseBone.name.casefold().startswith(right) or poseBone.name.casefold().endswith(right) for right in rightSide):
                poseBone.bone_group_index = 1
            else:
                poseBone.bone_group_index = 0

        #force edit mode
        StateUtility.SetEditMode()

        #select base armature
        bpy.ops.armature.select_all(action='DESELECT')
        bpy.ops.armature.select_all(action='SELECT') 

        for baseBone in bpy.context.selected_editable_bones:
            baseBone.use_connect = False
            
        #duplicate base armature. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #add .rig suffix to duplicate bones now known as rig bones.
        for copiedBone in bpy.context.selected_editable_bones: 
            copiedBone.name = copiedBone.name.replace(".001",".rig")

        #move rig bones to the second armature layer. 
        bpy.ops.armature.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        #make second armature layer visible and first layer hidden, to show only rig bones.
        bpy.context.object.data.layers[1] = True
        bpy.context.object.data.layers[0] = False

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')
        

        #change rig bones' display to circle, rotation mode to euler YZX and adds copy transform constraint to copy the base armature's animation.
        for rigBone in bpy.context.selected_pose_bones:
            rigBone.custom_shape = bpy.data.objects["Circle"]
            bpy.context.object.data.bones[rigBone.name].show_wire = True
            rigBone.rotation_mode = 'YZX'
            copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = rigBone.name.replace(".rig","")

        
        #bake rig bones animation so that they have the same animation as the base armature.
        StateUtility.BakeAnimation()

        #deselect all rig bones
        bpy.ops.pose.select_all(action='TOGGLE')

        #display base armature layer and hide rig armature layer 
        bpy.context.object.data.layers[0] = True
        bpy.context.object.data.layers[1] = False

        #select base armature
        bpy.ops.pose.select_all(action='SELECT')

        #base armature now follows rig armature
        for bone in bpy.context.selected_pose_bones:
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = bone.name + ".rig"
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #deselect base armature
        bpy.ops.pose.select_all(action='DESELECT')

        #show rig armature
        bpy.context.object.data.layers[1] = True
        bpy.context.object.data.layers[0] = False

        bpy.context.scene.frame_current = currentFrame #return to saved initial frame