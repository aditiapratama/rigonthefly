#########################################
#######       Rig On The Fly      #######
####### Copyright © 2020 Dypsloom #######
#######    https://dypsloom.com/  #######
#########################################

import bpy
from . BakeOnSkeleton import BakeOnSkeletonUtils

class BakeOnSkeletonOperator(bpy.types.Operator):
    bl_idname = "view3d.bake_on_skeleton_operator"
    bl_label = "Simple operator"
    bl_description = "Bake animation on the base skeleton and remove controllers"

    def execute(self, context):
        BakeOnSkeletonUtils.BakeOnSkeleton(self, context)
        return {'FINISHED'}